require 'rubygems'
require 'httparty'

class Banco
  include HTTParty
  base_uri ApiBancosUrl

  def self.bancos_all
    get("/api/v1/bancos")
  end

  def self.bancos_completo
    get("/api/v1/bancos/bancos_completo")
  end

  def self.plazos_y_tasas(banco_id)
    get("/api/v1/bancos/#{banco_id}/plazos_y_tasas")
  end

  def self.tasas(plazo_id)
    get("/api/v1/bancos/#{plazo_id}/tasas")
  end

  def self.calculo_monto(tasa = 0, monto = 0, plazo = 0)
    porcentaje_dia = tasa.to_f / 360

    monto_plazo = porcentaje_dia.to_f * monto.to_i * plazo.to_i
    monto_plazo.round(2) unless monto_plazo.blank?
  end


end