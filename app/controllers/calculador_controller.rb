class CalculadorController < ApplicationController

  def calcular
    @bancos = Banco.bancos_all
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def cargar_plazo_banco
    #obtengo el id del banco y consulto al ws
    @plazos_y_tasas = Banco.plazos_y_tasas(params[:banco_id])
    respond_to do |format|
      format.js # index.html.erb
    end
  end


  def calcular_monto
    #obtengo el id del banco y consulto al ws
    @tasa = Banco.tasas(params[:plazo])
    unless @tasa.blank?
      @monto_obtenido = Banco.calculo_monto @tasa['tasa'], params[:monto], params['plazo']
      @monto_total = (@monto_obtenido.to_f + params[:monto].to_f).round(2)
    end
    respond_to do |format|
      format.js # index.html.erb
    end
  end

end
